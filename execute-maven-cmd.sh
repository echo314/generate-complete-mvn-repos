#!/bin/bash

# gestion des parametres 

b_exe_mvn=0

while getopts ":ed" FLAG
do 
		case $FLAG in 
				e) b_exe_mvn=1 ; echo "Execute Maven Cmd" ;; 
				d) set -x ; echo "mode debug" ;;
				\?) echo "erreur" ; exit 2 ;; 
		esac 
done 

shift $((OPTIND-1))
if [[ $# -gt 1 ]]
then 
	echo "commande en trop"
	exit 3
fi 

# bloc fonction 

# execution des commandes maven
_exe_mvn_cmd()
{
	# on lance les commandes maven sur les projets 
	while read -r line  
	do  
		# on filtre la ligne en eliminant les lignes vide ou de types commentaires avec un #
		commande=$(echo "$line" | grep -E -v '^(#|$|[ *$])')
		
		# si la chaine est superieure a 0, on renseigne un nouvel objet de la collection
		if [[ -n "$commande" ]]
		then
			
			# on lance la commande sur un projet 
			mvn "$commande" -f "./project-ref/pom.xml" -s "./settings.xml"
	
		fi 
	done < ./lst-cmd-mvn.txt
}

echo "lancement du programme"

if [[ $b_exe_mvn -eq 1 ]] 
then 
	echo "execution cmd maven..."
	_exe_mvn_cmd
	echo "execution cmd maven...OK"
fi 

