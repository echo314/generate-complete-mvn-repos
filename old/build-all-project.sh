#!/bin/bash

# ajout d'un com fictif

# gestion des parametres 

b_gen_mvn_arc=0
b_exe_mvn_cmd=0
s_projets=""

echo "gestion des parametres"

while getopts ":gp:ad" FLAG
do 
		case $FLAG in 
				g) b_gen_mvn_arc=1 ; echo "param GEN ARCH" ;; 
				p) b_exe_mvn_cmd=1 ; echo "param ONE PROJ" ; s_projets=$OPTARG ;; 
				a) b_exe_mvn_cmd=1 ; echo "param LST PROJ" ; s_projets=$(ls | grep proj_) ;; 
				d) set -x ; echo "mode debug" ;;
				\?) echo "erreur" ; exit 2 ;; 
		esac 
done 

shift $((OPTIND-1))
if [[ $# -gt 1 ]]
then 
	echo "commande en trop"
	exit 3
fi 

echo $b_gen_mvn_arc
echo $b_exe_mvn_cmd


# bloc fonction 

# generation des archetypes 
_gen_mvn_arch(){
	i=0
	while read line  
	do  
		rm -rf ./proj_${i}
		
		# on filtre la ligne en eliminant les lignes vide ou de types commentaires avec un #
		commande=$( echo "$line" | grep -E -v '^(#|$|[ *$])' )
		
		
		# si la chaine est superieure a 0, on renseigne un nouvel objet de la collection 

		if [[ -n "$commande" ]]
		then
			
			groupe=($(echo "$commande" | cut -f1 -d":"))
			artifact=($(echo "$commande" | cut -f2 -d":"))
	
			# ----- objet -----------
			mvn -B archetype:generate \
				-DgroupId="mytest" \
				-DartifactId=proj_${i} \
				-Dversion="1.0" \
				-Dpackage="pkg" \
				-DarchetypeGroupId="${groupe}" \
				-DarchetypeArtifactId="${artifact}"
			
			i=$((i+1))
		fi 
	done < mvn-archetypes.txt
}

# execution des commandes maven
_exe_mvn_cmd()
{
	for projet in $s_projets
	do 
		echo "projet : ${projet}"
		# on lance les commandes maven sur les projets 
		while read line  
		do  
			# on filtre la ligne en eliminant les lignes vide ou de types commentaires avec un #
			commande=$(echo "$line" | grep -E -v '^(#|$|[ *$])')
			
			# si la chaine est superieure a 0, on renseigne un nouvel objet de la collection
			if [[ -n "$commande" ]]
			then
				
				# on lance la commande sur un projet 
				mvn $commande -f ${projet}/pom.xml
		
			fi 
		done < mvn-cmd.txt
		
		rm -rf $M2_HOME/repository/mytest/

	done 
}

echo "lancement du programme"

if [[ $b_gen_mvn_arc -eq 1 ]] 
then 
	echo "lancement generation archetype..."
	_gen_mvn_arch
	echo "lancement generation archetype...OK"
fi 

if [[ $b_exe_mvn_cmd -eq 1 ]] 
then 
	echo "lancement execution cmd maven..."
	_exe_mvn_cmd
	echo "lancement execution cmd maven...OK"
fi 


